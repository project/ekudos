## Readme for ekudos module

## About
Ekudos is a Dutch Diggalike democratic news system. This module adds an ekudos link to you posts
See ekudos.nl for the site.

This module has two theme funtions: theme_ekudos_add_link and theme_ekudos_vote_link, which can be used to change the appearance of the links.

After installation, make sure to switch the links on for the content types you want them on, at http://example.com/admin/settings/content-types

Requires either curl or fopen to work. If you have no clue what this means, you should probably contact sympal.nl (info@sympal.nl) for a managed hosting: we will sort out all such technical issues for you.

# About in Dutch
Ekudos is een Nederlandse digg-clone: een democratisch nieuwssysteem. Deze module voegt linkjes aan je artikelen toe waarmee mensn op kje kunnen stemmen (als het artikel is toegevoegd), of een artikel kunnen toevoegen.

De module heeft twee theme functies theme_ekudos_add_link and theme_ekudos_vote_link,waarmee je de weergave van de links kan aanpassen.

Na installatie moet je op http://example.com/admin/settings/content-types, voor ieder artikeltype dat je hebt en waar je de links wilt zien, het ekodus vakje aakruisen.

Deze module vereist curl of fopen. Als je geen flauw idee hebt wat dat is, of als je geen idee hebt hoe een Drupal module te installeren, kun je het beste contact opnemen met sympal.nl (info@sympal.nl) voor een Drupal site. Wij verzorgen dan alle technische rariteiten voor je.


## credits
By Bèr Kessels, webschuur.com ber@webschuur.com
For Sympal.nl
First released for Drupal 4.7
[Homepage](http://drupal.org/projects)
                                     _         _
     ___ _   _ _ __ ___  _ __   __ _| |  _ __ | |
    / __| | | | '_ ` _ \| '_ \ / _` | | | '_ \| |
    \__ \ |_| | | | | | | |_) | (_| | |_| | | | |
    |___/\__, |_| |_| |_| .__/ \__,_|_(_)_| |_|_|
         |___/          |_|

 -readme written in markdown-